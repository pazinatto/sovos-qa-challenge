Scenario: User should not be able to add invalid US Postal Code - 33000
Given I navigate to Home page
When I search and open Kindle Paperwhite Essentials Bundle including Kindle Paperwhite - Wifi with Special Offers, Amazon Leather Cover, and Power Adapter
And I enter US Postal Code 33000
Then I should be presented with alert message Please enter a valid US zip code


Scenario: User should not be able to add invalid US Postal Code - 99000
Given I navigate to Home page
When I search and open Kindle Paperwhite Essentials Bundle including Kindle Paperwhite - Wifi with Special Offers, Amazon Leather Cover, and Power Adapter
And I enter US Postal Code 99000
Then I should be presented with alert message Please enter a valid US zip code


Scenario: User should be able to add valid US Postal Code - 60601
Given I navigate to Home page
When I search and open Kindle Paperwhite Essentials Bundle including Kindle Paperwhite - Wifi with Special Offers, Amazon Leather Cover, and Power Adapter
And I enter US Postal Code 60601
Then The code that I entered 60601 is successfully added


Scenario: User should not be able to add invalid product quantity to the Purchase cart
Given I navigate to Home page
When I search for product Kindle Paperwhite Essentials Bundle including Kindle Paperwhite - Wifi with Special Offers, Amazon Leather Cover, and Power Adapter
And I open the product Kindle Paperwhite Essentials Bundle including Kindle Paperwhite - Wifi with Special Offers, Amazon Leather Cover, and Power Adapter and select desired quantity of 31+
Then I should not be able to include this product with the quantity desired


Scenario: User should be able to add valid product quantity to the Purchase cart
Given I navigate to Home page
When I search for product Kindle Paperwhite Essentials Bundle including Kindle Paperwhite - Wifi with Special Offers, Amazon Leather Cover, and Power Adapter
And I open the product Kindle Paperwhite Essentials Bundle including Kindle Paperwhite - Wifi with Special Offers, Amazon Leather Cover, and Power Adapter and select desired quantity of 15
Then I should be able to include the product in the shopping cart