Scenario: User should receive status code 404 when trying to retrieve an inexisting user 
Given I send a request to retrieve information of user ID <idUser>
Then I should receive a <statusCode> response

Examples:
|idUser	|statusCode	|
|15		|404		|
|999	|404		|


Scenario: I should be able to register a new user in the system
Given I create a request to create a report for <idUser>, <userName> and <password>
Then I should receive a <statusCode> response

Examples:
|idUser	|userName	|password	|statusCode	|
|1		|User 1		|Password1 	|200		|
|2		|User 2		|Password2 	|200		|


Scenario: User should be able to retrieve user information registered in the system
Given I send a request to retrieve information of user ID <idUser>
Then I should receive <idUser>, <userName> and <password> in the response
And I should receive a <statusCode> response

Examples:
|idUser	|userName	|password	|statusCode	|
|4		|User 4		|Password4 	|200		|
|6		|User 6		|Password6 	|200		|