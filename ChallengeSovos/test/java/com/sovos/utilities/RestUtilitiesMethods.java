package com.sovos.utilities;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

public class RestUtilitiesMethods {


	public static ResponseEntity<String> runPostRequest(String baseUrl, String id, String userName, String password){

		try {
			HttpEntity<Map<String, Object>> payload = new HttpEntity<>(createJsonPayload(id, userName, password));

			CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
			requestFactory.setHttpClient(httpClient);
			RestTemplate restTemplate = new RestTemplate(requestFactory);
			ResponseEntity<String> response = restTemplate.postForEntity(baseUrl, payload, String.class);
			return response;
		} catch (RestClientResponseException e) {
			return ResponseEntity
					.status(e.getRawStatusCode())
					.body(e.getResponseBodyAsString());
		}

	}

	public static ResponseEntity<String> runGetRequest(String baseUrl) {
		try {
			CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
			requestFactory.setHttpClient(httpClient);
			RestTemplate restTemplate = new RestTemplate(requestFactory);
			ResponseEntity<String> response = restTemplate.getForEntity(baseUrl, String.class);
			return response;
		} catch (RestClientResponseException e) {
			return ResponseEntity
					.status(e.getRawStatusCode())
					.body(e.getResponseBodyAsString());
		}
	}

	private static Map<String, Object> createJsonPayload(String id, String userName, String password){
		Map<String, Object> userData = new HashMap<>();
		userData.put("ID", id);
		userData.put("UserName", userName);
		userData.put("Password", password);

		return userData;
	}
}
