package com.sovos.steps;

import static org.junit.Assert.assertEquals;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.steps.Steps;
import org.springframework.http.ResponseEntity;

import com.jayway.restassured.path.json.JsonPath;
import com.sovos.utilities.RestUtilitiesMethods;


public class RestStepsClass extends Steps {
	
	final static String BASE_URL = "http://fakerestapi.azurewebsites.net/api/Users";
	
	ResponseEntity<String> responseSteps = null;
	Boolean isResponseFound = false;

	
	@Given("I create a request to create a report for $idUser, $userName and $password")
	public void createReportRequestWithoutToken(@Named("idUser") String idUser,
												@Named("userName") String userName,
												@Named("password") String password) {
		responseSteps = RestUtilitiesMethods.runPostRequest(BASE_URL, idUser, userName, password);
	}
	
	@Given("I send a request to retrieve information of user ID $idUser")
	public void sendGetRequest(@Named("idUser") String idUser) {
		responseSteps = RestUtilitiesMethods.runGetRequest(BASE_URL + "/" + idUser);
	}
	
	@Then("I should receive a $responseCode response")
	public void validateResponse(@Named("responseCode") int responseCode) {
		assertEquals(responseCode, responseSteps.getStatusCodeValue());;
	}
	
	@Then("I should receive $idUser, $userName and $password in the response")
	public void validateGetResponseUserInformation(@Named("idUser") String idUser,
													@Named("userName") String userName,
													@Named("password") String password) {
		assertEquals(idUser, JsonPath.from(responseSteps.getBody()).get("ID").toString());
		assertEquals(userName, JsonPath.from(responseSteps.getBody()).get("UserName"));
		assertEquals(password, JsonPath.from(responseSteps.getBody()).get("Password"));
		
	}
}
