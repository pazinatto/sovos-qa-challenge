package com.sovos.steps;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.jbehave.core.annotations.AfterStory;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.sovos.pages.HomePage;
import com.sovos.pages.ProductPage;
import com.sovos.pages.SearchResultPage;

public class WebStepsClass extends Steps {

	private static WebDriver driver;

	@BeforeScenario()
	public void beforeScenario() {
		if (driver == null) {
			System.setProperty("webdriver.chrome.driver","test/java/chromedriver.exe");		
			ChromeOptions options = new ChromeOptions();
			DesiredCapabilities capabilities = new DesiredCapabilities();
			options.addArguments("--disable-extensions");
			options.addArguments("--incognito");
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			options.merge(capabilities);
			driver = new ChromeDriver(options);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}

	}
	
	@Given("I navigate to Home page")
	public void navigateToHome() {
		driver.get("https://www.amazon.com/");
	}
	
	@When("I search for product $productName")
	public void searchForProduct(@Named("productName") String productName) {
		HomePage homePage = new HomePage(driver);
		homePage.searchForProduct(productName);
	}
	
	@When("I search and open $productName")
	public void searchForProductAndOpenIt(@Named("productName") String productName) {
		HomePage homePage = new HomePage(driver);
		homePage.searchForProduct(productName);
		
		SearchResultPage searchResultPage = new SearchResultPage(driver);
		searchResultPage.openProductDetails(productName);
	}
	
	@When("I open the product $productName and select desired quantity of $quantitySelected")
	public void selectProductAndQuantity(@Named("productName") String productSelected,
										@Named("quantitySelected") String quantitySelected) {
		SearchResultPage searchResultPage = new SearchResultPage(driver);
		searchResultPage.openProductDetails(productSelected);
		
		ProductPage productPage = new ProductPage(driver);
		productPage.selectProductQuantity(quantitySelected);
	}
	
	@When("I enter US Postal Code $usPostalCode")
	public void enterUsPostalCode(@Named("usPostalCode") String usPostalCode) {
		ProductPage productPage = new ProductPage(driver);
		productPage.changePostalCode(usPostalCode);
	}
	
	@Then("I should not be able to include this product with the quantity desired")
	public void validateQuantitySelected() {
		ProductPage productPage = new ProductPage(driver);
		assertEquals("Standard Amazon accounts are limited to fewer than 31 units. Larger quantities "
						+ "may be ordered with an Amazon Business account. Sign in with or create an "
						+ "Amazon Business account to continue. Amazon does not offer retail promotional"
						+ " discounts for bulk device orders and all bundle offers involving Amazon devices"
						+ " are limited to one per customer.", productPage.invalidQuantitySelected());
	}
	
	@Then("I should be able to include the product in the shopping cart")
	public void clickOnBuyNow() {
		ProductPage productPage = new ProductPage(driver);
		productPage.addProductToCart();
		
		assertEquals("Sign-In", productPage.returnSignInLabel());
	}
	
	@Then("I should be presented with alert message $productAlertMessage")
	public void validateIncorrectUsPostalCodeEntered(@Named("productAlertMessage") String productAlertMessage) {
		ProductPage productPage = new ProductPage(driver);
		String alertMessage = productPage.returnUsPostalCodeAlertMessage();
		
		assertEquals(productAlertMessage, alertMessage);
	}
	
	@Then("The code that I entered $usPostalCodeExpected is successfully added")
	public void validateCorrectUsPostalCodeEntered(@Named("usPostalCodeExpected") String usPostalCodeExpected) {
		ProductPage productPage = new ProductPage(driver);
		String usPostalCodeAdded = productPage.returnUsPostalCodeSuccessfullyAdded();
		
		assertEquals(usPostalCodeExpected, usPostalCodeAdded);
	}
	
	
	@AfterStory
	public void afterStory() {
		driver.quit();
	}

}
