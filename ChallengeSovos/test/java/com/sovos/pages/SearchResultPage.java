package com.sovos.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.sovos.utilities.WebUtilitiesMethods;

public class SearchResultPage {
	private WebDriver driver;

	public SearchResultPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void openProductDetails(String productSelected) {
		WebUtilitiesMethods.clickOnElement(driver, By.xpath("//div[@class='sg-col-inner']/div/h2/a/span[contains(text(),'" + productSelected + "')]"));
	}
}
