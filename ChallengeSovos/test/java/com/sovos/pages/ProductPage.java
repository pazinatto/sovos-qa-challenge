package com.sovos.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import com.sovos.utilities.WebUtilitiesMethods;

public class ProductPage {
	private WebDriver driver;

	public ProductPage(WebDriver driver) {
		this.driver = driver;
	}

	By productQuantity = By.id("quantity");
	By invalidQuantityMessage = By.xpath("//*[@class='a-popover-inner']");
	By deliverToButton = By.id("contextualIngressPtLabel");
	By usPostalCodeField = By.id("GLUXZipUpdateInput");
	By applyPostalCodeButton = By.cssSelector("#GLUXZipUpdate > span > input");
	By postalCodeAlertMessage = By.cssSelector("#GLUXZipError > div > div > div");
	By usPostalCodeSuccessfullyApplied = By.id("GLUXHiddenSuccessSelectedAddressPlaceholder");
	By buyNowButton = By.id("buy-now-button");
	By signInLabel = By.cssSelector("div.a-section > form > div > div > div > h1");	


	public void selectProductQuantity(String quantity) {
		new Select(driver.findElement(productQuantity)).selectByVisibleText(quantity);
	}

	public String invalidQuantitySelected() {
		return WebUtilitiesMethods.returnTextElement(driver, invalidQuantityMessage);
	}

	public void changePostalCode(String usPostalCodeChoice) {
		WebUtilitiesMethods.waitForMiliseconds(1000);
		WebUtilitiesMethods.clickOnElement(driver, deliverToButton);
		WebUtilitiesMethods.waitForMiliseconds(1000);
		WebUtilitiesMethods.fillElement(driver, usPostalCodeField, usPostalCodeChoice);
		WebUtilitiesMethods.clickOnElement(driver, applyPostalCodeButton);
	}

	public String returnUsPostalCodeAlertMessage() {
		return WebUtilitiesMethods.returnTextElement(driver, postalCodeAlertMessage);
	}

	public String returnUsPostalSuccessfullyAdded() {
		return WebUtilitiesMethods.returnTextElement(driver, usPostalCodeSuccessfullyApplied);
	}

	public String returnUsPostalCodeSuccessfullyAdded() {
		WebUtilitiesMethods.waitForMiliseconds(1000);
		return WebUtilitiesMethods.returnTextElement(driver, usPostalCodeSuccessfullyApplied);
	}

	public void addProductToCart() {
		WebUtilitiesMethods.clickOnElement(driver, buyNowButton);
	}

	public String returnSignInLabel() {
		return WebUtilitiesMethods.returnTextElement(driver, signInLabel);
	}

}
