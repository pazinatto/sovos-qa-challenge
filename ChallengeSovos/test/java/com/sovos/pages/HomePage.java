package com.sovos.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.sovos.utilities.WebUtilitiesMethods;

public class HomePage {
	private WebDriver driver;

	public HomePage(WebDriver driver) {
		this.driver = driver;
	}

	By searchBox = By.id("twotabsearchtextbox");
	By searchButton = By.className("nav-input");

	public void searchForProduct(String productName) {
		WebUtilitiesMethods.fillElement(driver, searchBox, productName);
		WebUtilitiesMethods.waitForMiliseconds(1000);
		WebUtilitiesMethods.clickOnElement(driver, searchButton);

	}
}
